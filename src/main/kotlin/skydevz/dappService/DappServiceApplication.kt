package skydevz.dappService
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class SpringBootKotlinApp

fun main(args: Array<String>)
{
	SpringApplication.run(SpringBootKotlinApp::class.java, *args)
	println("working")
}
