package skydevz.dappService.cart

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
@Table
data class CartItem(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var cartID: Long,
        @Column var itemID: Long,
        @Column var count: Int,
        @Column var isDelivered: Boolean = false
)