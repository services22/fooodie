package skydevz.dappService.cart

import skydevz.dappService.common.SDRole
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
@Table
data class Cart(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column val shopID: String,
        @Column var placeID: Long,
        @Column var orderedBy: SDRole,
        @Column var userID: String,
        @Column var status: Status = Status.NOT_DELIVERED
) {
        enum class Status {
                NOT_DELIVERED, DELIVERED //, CHECKED_OUT, PARTIALLY_DELIVERED
        }
}

