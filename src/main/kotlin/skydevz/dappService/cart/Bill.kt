package skydevz.dappService.cart

import java.util.Date
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
@Table
data class Bill (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var shopID: String,
        @Column var cartID: Long,
        @Column var timestamp: Date,
        @Column var netAmount: Double,
        @Column var taxAmount: Double,
        @Column var totalAmount: Double,
        @Column var billNumber: String
)
