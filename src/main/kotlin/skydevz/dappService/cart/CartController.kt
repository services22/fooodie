package skydevz.dappService.cart

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import skydevz.dappService.shop.Shop
import skydevz.dappService.tax.ParentType
import skydevz.dappService.common.URLpath
import java.util.Date
import java.util.UUID
import javax.validation.Valid
import kotlin.collections.HashMap
import skydevz.dappService.shop.TaxingType
import skydevz.dappService.common.*
import skydevz.dappService.place.Place
import skydevz.dappService.security.SDUser

@RestController
@RequestMapping(URLpath.api)

class CartController(private val cartRepository: CartRepository,
                     private val cartItemRepository: CartItemRepository,
                     private val placeRepository: PlaceRepository,
                     private val itemRepository: ItemRepository,
                     private val taxRepository: TaxRepository,
                     private val categoryRepository: CategoryRepository,
                     private val billRepository: BillRepository,
                     private val sdUserRepository: SDUserRepository,
                     private val staffRepository: StaffRepository,
                     shopRepository: ShopRepository) : BaseShopController(shopRepository) {

    @GetMapping(URLpath.server + URLpath.carts)
    fun getCarts(@PathVariable shopID: String, @RequestParam(required = false) status: Cart.Status?): List<CartDTO> {
        status?.let { cartStatus ->
            val carts = cartRepository.getCartsByShopID(shopID).mapNotNull { getCartDTO(it) }
            return carts.filter { it.cart.status == cartStatus }
        }
        return cartRepository.getCartsByShopID(shopID).mapNotNull { getCartDTO(it) }
    }

    @GetMapping(URLpath.userCarts)
    fun getCartsForUser(@RequestParam(required = false) status: Cart.Status?): List<CartDTO> {
        val user = Helper.getUser()!!
        status?.let { cartStatus ->
            val carts = cartRepository.getCartsByUserID(user.id).mapNotNull { getCartDTO(it) }
            return carts.filter { it.cart.status == cartStatus }
        }
        return cartRepository.getCartsByUserID(user.id).mapNotNull { getCartDTO(it) }
    }

    @PostMapping(URLpath.addItems)
    fun addItemsToCart(@PathVariable shopID: String, @PathVariable placeID: Long,
                       @RequestParam(required = false) cartID: Long?,
                       @RequestBody items: HashMap<Long, Int>): ResponseEntity<CartWithItems> {

        val user = Helper.getUser() ?: return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
        checkUserPersmission(user, shopID)?.let { return it.build() }
        checkPlaceIsInShop(shopID, placeID)?.let { return it.build() }

        var place = placeRepository.getById(placeID)
                ?: return ResponseEntity.badRequest().header(Error.headerKey, Error.placeNotFound).build()

        if (place.shopID != shopID) {
            return ResponseEntity.badRequest().header(Error.headerKey, Error.shopMismatch).build()
        }

        getCart(cartID)?.let {
            if (it.placeID != placeID) {
                return ResponseEntity.badRequest().header(Error.headerKey, Error.placeMismatch).build()
            }

            return addItemsToCart(items, it)
        }

        return saveItemsInCart(items, shopID, place, user)
    }

//    @GetMapping(URLpath.cart)
//    fun getCartById(@PathVariable shopID: String,@PathVariable id: Long): Cart {
//        return cartRepository.getOne(id)
//    }

    @GetMapping(URLpath.cart)
    fun getItemsInCart(@PathVariable shopID: String, @PathVariable id: Long): ResponseEntity<CartWithItems> {
        val user = Helper.getUser() ?: return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
        checkUserPersmission(user, shopID)?.let { return it.build() }
        return ResponseEntity.ok(getCartWithItems(id))
    }

    @PutMapping(URLpath.cart)
    fun updateCartById(@PathVariable shopID: String, @PathVariable id: Long,
                       @Valid @RequestBody newCart: Cart): Cart {
        val cart = cartRepository.getOne(id)
        val updatedCart = cart.copy(shopID = newCart.shopID, placeID = newCart.placeID,
                orderedBy = newCart.orderedBy, userID = newCart.userID, status = newCart.status)
        return cartRepository.save(updatedCart)
    }

    @DeleteMapping(URLpath.cart)
    fun deleteCart(@PathVariable shopID: String, @PathVariable id: Long) {
        return cartRepository.deleteById(id)
    }

    @PutMapping(URLpath.cartItem)
    fun updateCartItem(@PathVariable shopID: String, @PathVariable cartID: Long,
                       @PathVariable id: Long, @Valid @RequestBody newCartItem: CartItem): CartItem {
        val cartItem = cartItemRepository.getOne(id)
        val updatedCartItem = cartItem.copy(cartID = newCartItem.cartID, itemID = newCartItem.itemID,
                count = newCartItem.count, isDelivered = newCartItem.isDelivered)
        return cartItemRepository.save(updatedCartItem)
    }

    @DeleteMapping(URLpath.cartItem)
    fun deleteCartItem(@PathVariable shopID: String, @PathVariable cartID: Long,
                       @PathVariable id: Long) {
        return cartItemRepository.deleteById(id)
    }

    @PatchMapping(URLpath.changePlace)
    fun changePlace(@PathVariable shopID: String, @PathVariable cartID: Long, @RequestParam newPlaceId: Long): ResponseEntity<Void> {
        return changePlace(cartID, newPlaceId)
    }

    @PatchMapping(URLpath.server + URLpath.itemDeliver)
    fun deliverCartItem(@PathVariable shopID: String, @PathVariable id: Long,
                        @RequestParam isDelivered: Boolean): ResponseEntity<Void> {
        // check if the server has access to the shop

        // provide option only for admin to undeliver

        if (cartItemRepository.updateDeliveryStatus(id, isDelivered) == 1) {
            return ResponseEntity.ok().build()
        }

        return ResponseEntity.badRequest().build()
    }

    @GetMapping(URLpath.bill)
    fun checkout(@PathVariable shopID: String, @PathVariable cartID: Long): ResponseEntity<Bill> {

        getBill(shopID, cartID)?.let {
            return ResponseEntity.ok(it)
        }
        return ResponseEntity.notFound().build()
    }

    private fun checkPlaceIsInShop(shopID: String, placeID: Long): ResponseEntity.BodyBuilder? {
        placeRepository.getById(placeID)?.let {
            if (it.shopID != shopID) {
                return ResponseEntity.badRequest().header(Error.headerKey, Error.shopMismatch)
            }

            return null
        }

        return ResponseEntity.badRequest().header(Error.headerKey, Error.placeNotFound)
    }

    private fun addItemsToCart(items: HashMap<Long, Int>, cart: Cart): ResponseEntity<CartWithItems> {
        var cartDTO = getCartWithItems(cart.id)

        for ((itemID, count) in items) {
            var item = itemRepository.getById(itemID)
                    ?: return ResponseEntity.badRequest().header(Error.headerKey, Error.itemNotFound).build()

            if (item.shopID != cart.shopID) {
                return ResponseEntity.badRequest().header(Error.headerKey, Error.shopMismatch).build()
            }

            var cartItem = cartItemRepository.save(CartItem(0, cart.id, itemID, count))

            cartDTO.items.add(CartItemDTO(cartItem.id, item, count, false))

        }

        return when (val result = getCartItems(items, cart.shopID, cart.id)) {
            is Success -> {
                cartDTO.items.addAll(result.value)
                ResponseEntity.ok(cartDTO)
            }
            is Failure -> result.error.build()
        }


    }

    private fun saveItemsInCart(items: HashMap<Long, Int>, shopID: String, place: Place, user: SDUser): ResponseEntity<CartWithItems> {

        val cart = cartRepository.save(Cart(0, shopID, place.id, user.getRole(), user.id))
        return when (val result = getCartItems(items, shopID, cart.id)) {
            is Success -> ResponseEntity.ok(CartWithItems(cart, result.value, user.info(), place))
            is Failure -> result.error.build()
        }
    }

    private fun getCartItems(items: HashMap<Long, Int>, shopID: String, cartID: Long): Result<ArrayList<CartItemDTO>, ResponseEntity.BodyBuilder> {
        var cartItems = arrayListOf<CartItemDTO>()

        for ((itemID, count) in items) {
            var item = itemRepository.getById(itemID)
                    ?: return Failure(ResponseEntity.badRequest().header(Error.headerKey, Error.itemNotFound))

            if (item.shopID != shopID) {
                return Failure(ResponseEntity.badRequest().header(Error.headerKey, Error.shopMismatch))
            }

            var cartItem = cartItemRepository.save(CartItem(0, cartID, itemID, count))

            cartItems.add(CartItemDTO(cartItem.id, item, count, false))

        }
        return Success(cartItems)
    }

    private fun checkUserPersmission(user: SDUser, shopID: String): ResponseEntity.BodyBuilder? {
        if (user.roles.contains(SDRole.USER.name)) {
            return null
        }

        var adminID = getAdminID(user) ?: return ResponseEntity.status(HttpStatus.FORBIDDEN)

        checkShopExists(shopID)?.let { return it }

        if (adminID == shopRepository.getOne(shopID).adminID) {
            return null
        }

        return ResponseEntity.status(HttpStatus.FORBIDDEN)
    }

    private fun getAdminID(user: SDUser): String? {
        if (user.roles.contains(SDRole.ADMIN.name)) {
            return user.id
        }

        return staffRepository.getById(user.id)?.adminID
    }
    private fun canAccessCart(user: SDUser): Boolean {
        if (user.roles.contains(SDRole.SERVER.name)) {
            return true
        } else if (user.roles.contains(SDRole.USER.name)) {
            return true
        }

        return false
    }

    private fun getCartWithItems(cartID: Long): CartWithItems {
        var deliveredItems = 0
        val totalItems = cartItemRepository.getCartItemsByCartID(cartID)

        val cartItems = arrayListOf<CartItemDTO>()
        totalItems.forEach {
            if (it.isDelivered) {
                deliveredItems += 1
            }
            cartItems.add(CartItemDTO(it.id, itemRepository.getOne(it.itemID), it.count, it.isDelivered))
        }

        val cart = cartRepository.getOne(cartID)
        cart.status = getCartStatus(totalItems.count(), deliveredItems)

        return CartWithItems(cart, cartItems,
                sdUserRepository.getOne(cart.userID).info(),
                placeRepository.getOne(cart.placeID))
    }

    private fun getCartDTO(cart: Cart): CartDTO? {
        val user = sdUserRepository.getById(cart.userID) ?: return null
        var place = placeRepository.getById(cart.placeID) ?: return null
        val cartItems = cartItemRepository.getCartItemsByCartID(cart.id)
        val result = CartDTO(cart, cartItems.count(), 0,
                user.info(), place)
        cartItems.forEach {
            if (it.isDelivered) {
                result.deliveredItems += 1
            }
        }

        result.cart.status = getCartStatus(cartItems.count(), result.deliveredItems)
        return result
    }

    private fun getCartStatus(totalItems: Int, deliveredItems: Int): Cart.Status {
        if (totalItems == deliveredItems) {
            return Cart.Status.DELIVERED
//        } else if (deliveredItems > 0) {
//            return Cart.Status.PARTIALLY_DELIVERED
        }

        return Cart.Status.NOT_DELIVERED
    }

    private fun changePlace(cartID: Long, newPlaceId: Long): ResponseEntity<Void> {

        var updatedRows = 0
        var cart = cartRepository.getOne(cartID)
        var oldPlaceId = cart.placeID
        updatedRows += placeRepository.updateAvailablity(oldPlaceId, true)
        updatedRows += placeRepository.updateAvailablity(newPlaceId, false)
        updatedRows += cartRepository.updatePlace(cartID, newPlaceId)

        if (updatedRows == 3) {
            return ResponseEntity.ok().build()
        }

        return ResponseEntity.badRequest().build()
    }

    private fun getBill(shopID: String, cartID: Long): Bill? {
        shopRepository.getById(shopID)?.let {
            return getBill(it, cartID)
        }

        return null
    }

    private fun getBill(shop: Shop, cartID: Long): Bill {

        var cartItems = cartItemRepository.getCartItemsByCartID(cartID)
        if (cartItems.count() == 0) {
            throw IllegalArgumentException()
        }

        var items: HashMap<Long, Int> = hashMapOf()
        for (cartItem in cartItems) {
            items[cartItem.itemID]?.let {
                items[cartItem.itemID] = it + cartItem.count
            } ?: kotlin.run {
                items[cartItem.itemID] = cartItem.count
            }
        }

        var tax = 0.0
        var total = 0.0
        var subTotal = 0.0
        val timestamp = Date()
        val billNumber = UUID.randomUUID().toString()
        val bill = Bill(0, shop.id, cartID, timestamp, subTotal, tax, total, billNumber)

        for (item in items) {
            calculateAmount(shop, bill, item.key, item.value)
        }

        makePlaceAvailable(cartID)
        return billRepository.save(bill)

    }

    private fun makePlaceAvailable(cartID: Long) {

        val cart = cartRepository.getOne(cartID)
        val place = placeRepository.getOne(cart.placeID)
        place.isAvailable = true
        placeRepository.save(place)
    }

    private fun calculateAmount(shop: Shop, bill: Bill, itemID: Long, itemCount: Int) {

        var netAmount: Double
        var item = itemRepository.getOne(itemID)

        var itemPrice = item.price
        netAmount = itemPrice * itemCount
        bill.netAmount += netAmount

        var taxes =
                when (shop.taxBasedOn) {
                    TaxingType.SHOP -> taxRepository.getTaxesByParentTypeAndParentID(ParentType.SHOP, shop.id)
                    TaxingType.CATEGORY -> taxRepository.getTaxesByParentTypeAndParentID(ParentType.CATEGORY, categoryRepository.getOne(item.categoryID).id.toString())
                    TaxingType.ITEM -> taxRepository.getTaxesByParentTypeAndParentID(ParentType.ITEM, itemID.toString())
                }

        for (eachTax in taxes) {
            var taxAmount = (netAmount * eachTax.percentage) / 100
            bill.taxAmount += taxAmount
            taxAmount = 0.0
        }

        bill.totalAmount = bill.netAmount + bill.taxAmount

    }

    private fun getCart(cartID: Long?): Cart? {
        cartID?.let {
            cartRepository.getById(it)?.let { cart ->
                return cart
            }
        }

        return null
    }
}

fun SDUser.info(): UserInfo = UserInfo(this.userName, this.imageUrl)


