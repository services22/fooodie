package skydevz.dappService.item

import skydevz.dappService.common.TimeStampedEntity
import java.util.*
import javax.persistence.*

@Entity
@Table
data class Item(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var shopID: String,
        @Column var categoryID: Long,
        @Column var name: String,
        @Column var price: Double,
        @Column var imageURL: String?,
        @Column var isAvailable: Boolean = true,
        @Column var code: String?
) : TimeStampedEntity()