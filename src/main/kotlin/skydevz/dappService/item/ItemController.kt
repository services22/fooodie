package skydevz.dappService.item

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import skydevz.dappService.common.*
import javax.validation.Valid

@RestController
@RequestMapping(URLpath.api)

class ItemController(private val itemRepository: ItemRepository,
                     private val categoryRepository: CategoryRepository,
                     shopRepository: ShopRepository) : BaseShopController(shopRepository) {

    @GetMapping(URLpath.categoryItems)
    fun getItemsByShopIdAndCategoryId(@PathVariable shopID: String,
                                      @PathVariable categoryId: Long): List<Item> {
        return itemRepository.getItemByShopIDAndCategoryID(shopID, categoryId)
    }

    @GetMapping(URLpath.shopItems)
    fun getItemsInShop(@PathVariable shopID: String,
                       @RequestParam(required = false) isAvailable: Boolean?): List<Item> {
        isAvailable?.let {
            return itemRepository.getItemsByShopIDAndIsAvailable(shopID, it)
        }

        return itemRepository.getItemsByShopID(shopID)
    }

    @PostMapping(URLpath.admin + URLpath.categoryItems)
    fun createItem(@PathVariable shopID: String,
                   @PathVariable categoryID: Long,
                   @Valid @RequestBody item: Item): ResponseEntity<Item> {
        checkShop(shopID, item.shopID)?.let { return it.build() }


        if (categoryID != item.categoryID) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).header(Error.headerKey, Error.categoryMismatch).build()
        }

        checkCategoryExists(categoryID)?.let { return it.build() }

        return ResponseEntity.ok(itemRepository.save(item))
    }

    @GetMapping(URLpath.categoryItem)
    fun getItemById(@PathVariable shopID: String, @PathVariable categoryId: Long, @PathVariable id: Long): Item {
        return itemRepository.getOne(id)
    }

    @PutMapping(URLpath.admin + URLpath.categoryItem)
    fun updateItemById(@PathVariable shopID: String, @PathVariable categoryID: Long, @PathVariable id: Long,
                       @Valid @RequestBody newItem: Item): ResponseEntity<Item> {
        checkShop(shopID, newItem.shopID)?.let { return it.build() }
        checkCategoryExists(categoryID)?.let { return it.build() }

        var item = itemRepository.getOne(id)
        var updatedItem = item.copy(shopID = newItem.shopID, categoryID = newItem.categoryID, name = newItem.name,
                imageURL = newItem.imageURL, price = newItem.price,
                isAvailable = newItem.isAvailable, code = newItem.code)

        return ResponseEntity.ok(itemRepository.save(updatedItem))
    }

    @PatchMapping(URLpath.admin + URLpath.shopItem)
    fun updateAvailability(@PathVariable shopID: String,
                           @PathVariable id: Long,
                           @RequestParam isAvailable: Boolean): ResponseEntity<Void> {
        checkShopExists(shopID)?.let { return it.build() }
        checkAdminAccess(shopID)?.let { return it.build() }

        if (itemRepository.updateAvailablity(id, isAvailable) == 1) {
            return ResponseEntity.ok().build()
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

    @DeleteMapping(URLpath.admin + URLpath.categoryItem)
    fun deleteItem(@PathVariable shopID: String, @PathVariable categoryId: Long, @PathVariable id: Long): ResponseEntity<Void> {
        checkShopExists(shopID)?.let { return it.build() }
        checkAdminAccess(shopID)?.let { return it.build() }
        itemRepository.deleteById(id)
        return ResponseEntity.ok().build()
    }

    private fun checkCategoryExists(categoryID: Long): ResponseEntity.BodyBuilder? {

        if (!categoryRepository.existsById(categoryID)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).header(Error.headerKey, Error.categoryNotFound)
        }

        return null
    }
}