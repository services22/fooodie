package skydevz.dappService.tax

import org.springframework.web.bind.annotation.*
import skydevz.dappService.common.CartItemRepository
import skydevz.dappService.common.CategoryRepository
import skydevz.dappService.common.ShopRepository
import skydevz.dappService.common.TaxRepository
import skydevz.dappService.common.URLpath
import java.lang.IllegalArgumentException
import javax.validation.Valid

@RestController
@RequestMapping(URLpath.api)

class TaxController(private val taxRepository: TaxRepository,
                    private val shopRepository: ShopRepository,
                    private val categoryRepository: CategoryRepository,
                    private val itemRepository: CartItemRepository) {

    @GetMapping(URLpath.taxes)
    private fun getTaxes(@PathVariable shopID: String, @RequestParam parentType: ParentType, @RequestParam parentId: String): List<Tax> {
        return taxRepository.getTaxesByParentTypeAndParentID(parentType, parentId)
    }

    @PostMapping(URLpath.taxes)
    private fun createTax(@PathVariable shopID: String,@Valid @RequestBody tax: Tax): Tax {
        var shop = shopRepository.getOne(shopID)
        checkTaxProperties(tax,shopID)
        return taxRepository.save(tax)
    }

//    @GetMapping(URLpath.tax)
//    private fun getTax(@PathVariable taxId: Long): ResponseEntity<Tax> {
//        return taxRepository.findById(taxId).map {
//            ResponseEntity.ok(it)
//        }.orElse(ResponseEntity.notFound().build())
//    }

    @PutMapping(URLpath.tax)
    private fun updateTax(@PathVariable shopID: String, @PathVariable taxId: Long,
                          @Valid @RequestBody newTax: Tax): Tax {
        var tax = taxRepository.getOne(taxId)
        var updatedTax = tax.copy(parentType = newTax.parentType, name = newTax.name, percentage = newTax.percentage)
        return taxRepository.save(updatedTax)
    }

    @DeleteMapping(URLpath.tax)
    private fun deleteTax(@PathVariable shopID: String, @PathVariable taxId: Long) {
        taxRepository.deleteById(taxId)
    }

    @DeleteMapping(URLpath.taxes)
    private fun deleteTaxes(@PathVariable shopID: String, @RequestParam taxes :List<Tax>) {
        for (tax in taxes) {
            taxRepository.deleteById(tax.id)
        }
    }

    private fun checkTaxProperties(tax: Tax,shopID: String) {
        if(tax.parentType == ParentType.SHOP) {
            if (shopID != tax.parentID) {
                throw IllegalArgumentException("shop id in the path and body did not match")
            }
        }
        else if(tax.parentType == ParentType.CATEGORY) {
            var category = categoryRepository.getOne(tax.parentID.toLong())
        }
        else if (tax.parentType == ParentType.ITEM) {
            var item = itemRepository.getOne(tax.parentID.toLong())
        }
    }

}