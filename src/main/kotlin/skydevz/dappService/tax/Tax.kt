package skydevz.dappService.tax

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType

@Entity
@Table
data class Tax(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var parentType: ParentType = ParentType.SHOP,
        @Column var parentID: String,
        @Column var name: String,
        @Column var percentage: Double

)

enum class ParentType {
    SHOP, ITEM, CATEGORY
}