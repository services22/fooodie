package skydevz.dappService.address

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import skydevz.dappService.common.*
import javax.validation.Valid

@RestController
@RequestMapping(URLpath.api)
class AddressController(private val addressRepository: AddressRepository,
                        shopRepository: ShopRepository): BaseShopController(shopRepository) {

    @GetMapping(URLpath.address)
    fun getAddresses(@PathVariable shopID: String): List<Address> {
        return addressRepository.getAddressListByShopID(shopID)
    }

    @PostMapping(URLpath.admin + URLpath.address)
    fun createAddress(@PathVariable shopID: String,
                      @Valid @RequestBody address: Address): ResponseEntity<Address> {
        checkShopAndAdminAccess(shopID, address)?.let { return it.build() }
        return ResponseEntity.ok(addressRepository.save(address))
    }

    @PutMapping(URLpath.admin + URLpath.address)
    fun updateAddressById(@PathVariable shopID: String, @PathVariable id: Long,
                          @Valid @RequestBody newAddress: Address): ResponseEntity<Address> {
        var address = addressRepository.getOne(id)
        checkShopAndAdminAccess(shopID, address)?.let { return it.build() }
        var updatedAddress = address.copy(shopID = newAddress.shopID, addressLineOne = newAddress.addressLineOne,
                addressLineTwo = newAddress.addressLineTwo, city = newAddress.city, state = newAddress.state,
                postalCode = newAddress.postalCode, latitude = newAddress.latitude,
                longitude = newAddress.longitude)
        return ResponseEntity.ok(addressRepository.save(updatedAddress))
    }

    @DeleteMapping(URLpath.admin + URLpath.address)
    fun deleteAddress(@PathVariable shopID: String, @PathVariable id: Long): ResponseEntity<Void> {
        checkShopAndAdminAccess(shopID, addressRepository.getOne(id))?.let { return it.build() }
        addressRepository.deleteById(id)
        return ResponseEntity(HttpStatus.OK)
    }

    private fun checkShopAndAdminAccess(shopID: String, address: Address): ResponseEntity.BodyBuilder? {
        if (shopID != address.shopID) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(Error.headerKey, Error.shopMismatch)
        }

        return checkAdminAccess(shopID)
    }
}