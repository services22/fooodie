package skydevz.dappService.address

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
@Table
data class Address (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var shopID: String,
        @Column var addressLineOne: String,
        @Column var addressLineTwo: String?,
        @Column var city: String,
        @Column var state: String,
        @Column var postalCode: Int,
        @Column var latitude: Double,
        @Column var longitude: Double
)