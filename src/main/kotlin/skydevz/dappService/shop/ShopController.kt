package skydevz.dappService.shop

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import skydevz.dappService.address.Address
import skydevz.dappService.common.GeoLocation
import skydevz.dappService.tax.ParentType
import skydevz.dappService.common.*
import javax.validation.Valid
import kotlin.collections.ArrayList

@CrossOrigin
@RestController
@RequestMapping(URLpath.api)

class ShopController(private val addressRepository: AddressRepository,
                     private val taxRepository: TaxRepository,
                     private val placeRepository: PlaceRepository,
                     private val categoryRepository: CategoryRepository,
                     private val staffRepository: StaffRepository,
                     shopRepository: ShopRepository): BaseShopController(shopRepository) {

    @GetMapping(URLpath.shop)
    fun getShops(): List<Shop> = shopRepository.findAll()

    @GetMapping(URLpath.shop + URLpath.id)
    fun getShop(@PathVariable id: String): Shop = shopRepository.getOne(id)

    @GetMapping(URLpath.staff + URLpath.shop)
    fun getMyShops(@RequestParam(required = false) modifiedTime: Long?): List<Shop> {
        val user = Helper.getUser()!!
        val role = user.getRole()
        if (role == SDRole.ADMIN) {
            return getShops(user.id, modifiedTime)
        }

        val staff = staffRepository.getOne(user.id)

        return getShops(staff.adminID, modifiedTime)
    }

    @GetMapping(URLpath.shopDetail)
    fun getShopDetail(@PathVariable id: String,
                      @RequestParam(name = "include", required = false) requiredItems: ArrayList<String>?): ShopDetail {
        return getShopDetail(shopRepository.getOne(id), requiredItems)
    }

    @PostMapping(URLpath.admin + URLpath.shop)
    fun createShop(@Valid @RequestBody shop: ShopRequest): ResponseEntity<Shop> {
        val user = Helper.getUser()!!
        val shops = shopRepository.getShopsByAdminID(user.id)
        if (shops.count() > 3) {
            return ResponseEntity.status(HttpStatus.CONFLICT).header(Error.headerKey, Error.maxShopLimit).build()
        }
        
        val newShop = Shop(name = shop.name, placeType = shop.placeType,
                            imageURL = shop.imageUrl, taxBasedOn = shop.taxBasedOn,
                            adminID = user.id)
        return ResponseEntity.ok(shopRepository.save(newShop))
    }

    @PutMapping(URLpath.admin + URLpath.shop + URLpath.id)
    fun updateShop(@PathVariable id: String,
                   @Valid @RequestBody newShop: Shop): ResponseEntity<Shop> {
        checkShop(id, newShop.id)?.let { return it.build() }
        val updatedShop = shopRepository.save(newShop)
        return ResponseEntity.ok(updatedShop)
    }

    @DeleteMapping(URLpath.admin + URLpath.shop + URLpath.id)
    fun deleteShop(@PathVariable id: String): ResponseEntity<Void> {
        shopRepository.deleteById(id)
        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping(URLpath.nearByShops)
    fun getPlacesShops(@RequestParam latitude: Double,
                       @RequestParam longitude: Double,
                       @RequestParam distance: Double,
                       @RequestParam unit: DistanceUnit): List<Address> {
        val location = GeoLocation.fromDegrees(latitude, longitude)

        val (latRange, lonRange) =
                when (unit) {
                    DistanceUnit.kilometers -> location.getRangesBasedOnKM(distance)
                    DistanceUnit.miles -> location.getRangesBasedOnMiles(distance)
                }

        return addressRepository.getAddressesByLatitudeBetweenAndLongitudeBetween(latRange.min, latRange.max, lonRange.min, lonRange.max)
    }

    enum class DistanceUnit {
        miles, kilometers
    }

    private fun getShops(adminID: String, modifiedTime: Long?): List<Shop> {
        modifiedTime?.let {
            return shopRepository.getShops(adminID, modifiedTime)
        }

        return  shopRepository.getShopsByAdminID(adminID)
    }

    private fun getShopDetail(shop: Shop, requiredItems: ArrayList<String>?): ShopDetail {
        val shopDetail = ShopDetail(shop)

        requiredItems?.let {
            updateShopDetail(it, shopDetail, shop)
        }
        return shopDetail
    }

    private fun updateShopDetail(requiredItems: ArrayList<String>, shopDetail: ShopDetail, shop: Shop) {

        if (requiredItems.contains("address")) {

            shopDetail.addresses = addressRepository.getAddressListByShopID(shop.id)
        }

        if (requiredItems.contains(("place"))) {

            shopDetail.places = placeRepository.getPlacesByShopID(shop.id)
        }

        if (requiredItems.contains("category")) {

            shopDetail.categories = categoryRepository.getCategoriesByShopID(shop.id)
        }

        if (requiredItems.contains("tax")) {

            shopDetail.taxes = taxRepository.getTaxesByParentTypeAndParentID(ParentType.SHOP, shop.id)
        }
    }
}
