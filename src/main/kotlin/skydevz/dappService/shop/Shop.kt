package skydevz.dappService.shop

import skydevz.dappService.common.TimeStampedEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table
data class Shop(
        @Id @Column var id: String = UUID.randomUUID().toString(),
        @Column var name: String,
        @Column var placeType: PlaceType = PlaceType.RESTAURANT,
        @Column var imageURL: String?,
        @Column var taxBasedOn: TaxingType = TaxingType.SHOP,
        @Column var adminID: String
) : TimeStampedEntity()

enum class PlaceType {
    RESTAURANT, HOTEL
}

enum class TaxingType {
    SHOP, CATEGORY, ITEM
}
