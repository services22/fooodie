package skydevz.dappService.place

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.PathVariable
import skydevz.dappService.common.*
import javax.validation.Valid
import kotlin.collections.ArrayList

@RestController
@RequestMapping(URLpath.api)
class PlaceController(private val placeRepository: PlaceRepository, shopRepository: ShopRepository): BaseShopController(shopRepository) {

    @GetMapping(URLpath.places)
    fun getPlaces(@PathVariable shopID: String, @RequestParam(required = false) isAvailable: Boolean?): List<Place> {
        val flag: Boolean = isAvailable?.let { it } ?: run { true }
        return placeRepository.getPlacesByShopIDAndIsAvailable(shopRepository.getOne(shopID).id, flag)
    }

    @PatchMapping(URLpath.selectPlace)
    fun selectPlace(@PathVariable shopID: String, @PathVariable id: Long): ResponseEntity<Void> {
        val updatedRows = placeRepository.updateAvailablity(id, true)
        if (updatedRows > 0) {
            return ResponseEntity.ok().build()
        }

        return ResponseEntity.badRequest().build()
    }

    @PostMapping(URLpath.admin + URLpath.places)
    fun createPlace(@PathVariable shopID: String, @Valid @RequestBody place: Place): ResponseEntity<Place> {

        checkShop(shopID, place.shopID)?.let { return it.build() }

        return ResponseEntity.ok(placeRepository.save(place))
    }

    @PostMapping(URLpath.admin + URLpath.postPlaces)
    fun createPlaceByParam(@PathVariable shopID: String, @RequestParam(name = "from") startValue: Int,
                           @RequestParam(name = "to") endValue: Int, @RequestParam type: Place.Type,
                           @RequestParam capacity: Int): ResponseEntity<List<Place>> {
        checkShopExists(shopID)?.let { return it.build() }
        checkAdminAccess(shopID)?.let { return it.build() }

        val places = ArrayList<Place>()
        for (number in startValue..endValue) {
            val place = Place(0, shopID, type, true, number, capacity)
            places.add(place)
        }
        return ResponseEntity.ok(placeRepository.saveAll(places))
    }

//    @GetMapping(URLpath.place)
//    fun getPlace(@PathVariable id: Long): ResponseEntity<Place> {
//        return placeRepository.findById(id).map {
//            ResponseEntity.ok(it)
//        }.orElse(ResponseEntity.notFound().build())
//    }

    @PutMapping(URLpath.admin + URLpath.place)
    fun updatePlace(@PathVariable shopID: String, @PathVariable id: Long,
                    @Valid @RequestBody newPlace: Place): ResponseEntity<Place> {
        checkAdminAccess(shopID)?.let { return it.build() }

        val place = placeRepository.getOne(id)
        val updatedPlace = place.copy(shopID = newPlace.shopID, type = newPlace.type, isAvailable = newPlace.isAvailable,
                number = newPlace.number, capacity = newPlace.capacity)
        return ResponseEntity.ok(placeRepository.save(updatedPlace))
    }

    @DeleteMapping(URLpath.admin + URLpath.place)
    fun deletePlace(@PathVariable shopID: String, @PathVariable id: Long): ResponseEntity<Void> {
        checkAdminAccess(shopID)?.let { return it.build() }

        placeRepository.deleteById(id)
        return ResponseEntity.ok().build()
    }
}