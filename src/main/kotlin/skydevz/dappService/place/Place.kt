package skydevz.dappService.place

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
@Table
data class Place(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var shopID: String,
        @Column var type: Type = Type.TABLE,
        @Column var isAvailable: Boolean = true,
        @Column var number: Int,
        @Column var capacity: Int
) {
    enum class Type {
        TABLE, ROOM
    }
}