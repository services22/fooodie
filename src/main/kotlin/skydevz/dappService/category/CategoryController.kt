package skydevz.dappService.category

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import skydevz.dappService.common.BaseShopController
import skydevz.dappService.common.CategoryRepository
import skydevz.dappService.common.Error
import skydevz.dappService.common.ShopRepository
import skydevz.dappService.common.URLpath
import java.lang.IllegalArgumentException
import javax.validation.Valid

@RestController
@RequestMapping(URLpath.api)

class CategoryController(private val categoryRepository: CategoryRepository,
                         shopRepository: ShopRepository): BaseShopController(shopRepository) {

    @GetMapping(URLpath.categories)
    fun getCategories(@PathVariable shopID: String): List<Category> {
        return categoryRepository.getCategoriesByShopID(shopID)
    }

    @PostMapping(URLpath.admin + URLpath.categories)
    fun createCategory(@PathVariable shopID: String, @Valid @RequestBody category: Category): ResponseEntity<Category> {
        checkShop(shopID, category.shopID)?.let { return  it.build() }
        return ResponseEntity.ok(categoryRepository.save(category))
    }

    @GetMapping(URLpath.category)
    fun getCategoryById(@PathVariable shopID: String, @PathVariable id: Long): Category {
        return categoryRepository.getOne(id)
    }

    @PutMapping(URLpath.admin + URLpath.category)
    fun updateCategoryById(@PathVariable shopID: String, @PathVariable id: Long,
                           @Valid @RequestBody newCategory: Category): ResponseEntity<Category> {
        checkShop(shopID, newCategory.shopID)?.let { return  it.build() }
        var category = categoryRepository.getOne(id)
        var updatedCategory = category.copy(shopID = newCategory.shopID, name = newCategory.name,
                imageURL = newCategory.imageURL)
        return ResponseEntity.ok(categoryRepository.save(updatedCategory))
    }

    @PatchMapping(URLpath.admin + URLpath.category)
    fun updateCategory(@PathVariable shopID: String,
                           @PathVariable id: Long,
                           @RequestParam(required = false) isAvailable: Boolean?): ResponseEntity<Void> {
        checkShopExists(shopID)?.let { return it.build() }
        checkAdminAccess(shopID)?.let { return it.build() }

        isAvailable?.let { return updateAvailability(id, it) }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

    @DeleteMapping(URLpath.admin + URLpath.category)
    fun deleteCategory(@PathVariable shopID: String, @PathVariable id: Long): ResponseEntity<Void> {
        checkShopExists(shopID)?.let { return  it.build() }
        checkAdminAccess(shopID)?.let { return  it.build() }

        categoryRepository.deleteById(id)
        return ResponseEntity(HttpStatus.OK)
    }

    private fun updateAvailability(categoryID: Long, isAvailable: Boolean): ResponseEntity<Void> {
        if (categoryRepository.updateAvailablity(categoryID, isAvailable) == 1) {
            return ResponseEntity.ok().build()
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

//    private fun updatePostion(categoryID: Long, position: Int): ResponseEntity<Void> {
//
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
//    }
}