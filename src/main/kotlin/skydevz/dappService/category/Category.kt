package skydevz.dappService.category

import skydevz.dappService.common.TimeStampedEntity
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
@Table
data class Category (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column var id: Long,
        @Column var shopID: String,
        @Column var name: String,
        @Column var isAvailable: Boolean = true,
        @Column var imageURL: String?,
        @Column var code: String?
//        @Column var position: Int?
) : TimeStampedEntity()