package skydevz.dappService.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service

import java.util.Date
import java.util.HashMap
import java.util.function.Function

@Service
class JwtUtil {

    private val AUTH_SECRET_KEY = "myAwesomeSecretForSigning"
    private val CONFIRMATION_TOKEN_KEY = "confirmingKey"

    fun extractSubject(token: String, key: String): String {
        return extractClaim(token, Function { it.subject }, key)
    }

    fun extractExpiration(token: String, key: String): Date {
        return extractClaim(token, Function { it.expiration }, key)
    }

    fun <T> extractClaim(token: String, claimsResolver: Function<Claims, T>, key: String): T {
        val claims = extractAllClaims(token, key)
        return claimsResolver.apply(claims)
    }

    private fun extractAllClaims(token: String, key: String): Claims {
        return Jwts.parser().setSigningKey(key).parseClaimsJws(token).body
    }

    private fun isTokenExpired(token: String, key: String): Boolean {
        return extractExpiration(token, key).before(Date())
    }

    fun generateToken(userDetails: UserDetails): String {
        val claims = HashMap<String, Any>()
        return createToken(claims, userDetails.username, 24, AUTH_SECRET_KEY)
    }

    fun confirmationToken(email: String): String {
        return createToken(hashMapOf(), email, 1, CONFIRMATION_TOKEN_KEY)
    }

    private fun createToken(claims: Map<String, Any>, subject: String, duration: Int, key: String): String {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(Date(System.currentTimeMillis()))
                .setExpiration(Date(System.currentTimeMillis() + 1000 * 60 * 60 * duration))
                .signWith(SignatureAlgorithm.HS256, key).compact()
    }

    fun validateToken(token: String, userDetails: UserDetails): Boolean {
        val username = extractSubject(token, AUTH_SECRET_KEY)
        return username == userDetails.username && !isTokenExpired(token, AUTH_SECRET_KEY)
    }

    fun getEmailFrom(token: String): String? {
        if (isTokenExpired(token, CONFIRMATION_TOKEN_KEY)) {
            return null
        }

        return extractSubject(token, CONFIRMATION_TOKEN_KEY)
    }

    fun getUserNameFrom(token: String): String {
        return extractSubject(token, AUTH_SECRET_KEY)
    }
}