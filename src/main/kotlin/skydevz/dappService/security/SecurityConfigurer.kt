package skydevz.dappService.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import skydevz.dappService.common.SDRole
import skydevz.dappService.common.URLpath


@Configuration
@EnableWebSecurity
class SecurityConfigurer(var userDetailsService: SDUserDetailsService,
                         var jwtRequestFilter: JwtRequestFilter) : WebSecurityConfigurerAdapter() {

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService)
    }

    @Bean @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity.csrf().disable()
                .authorizeRequests()
                .antMatchers("${URLpath.api}${URLpath.master}/**").hasAuthority(SDRole.MASTER.name)
                .antMatchers("${URLpath.api}${URLpath.admin}/**").hasAuthority(SDRole.ADMIN.name)
                .antMatchers("${URLpath.api}${URLpath.waiter}/**").hasAuthority(SDRole.WAITER.name)
                .antMatchers("${URLpath.api}${URLpath.server}/**").hasAuthority(SDRole.SERVER.name)
                .antMatchers("${URLpath.api}${URLpath.staff}/**").hasAnyAuthority(SDRole.ADMIN.name,
                        SDRole.WAITER.name, SDRole.SERVER.name)
                .antMatchers("${URLpath.api}${URLpath.user}/**").hasAuthority(SDRole.USER.name)
                .antMatchers(URLpath.deactivate).authenticated()
                .anyRequest().permitAll()
                .and().exceptionHandling()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)

    }


}
