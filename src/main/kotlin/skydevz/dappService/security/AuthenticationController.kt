package skydevz.dappService.security

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import skydevz.dappService.common.*
import java.net.URI
import javax.validation.constraints.Email

@Validated
@RestController
@RequestMapping(URLpath.api)
class AuthenticationController(val authenticationManager: AuthenticationManager,
                               val jwtTokenUtil: JwtUtil,
                               val userDetailsService: SDUserDetailsService,
                               var sdUserRepository: SDUserRepository,
//                               var emailService: EmailService,
                               val javaMailSender: JavaMailSender) {

    @GetMapping("/health")
    fun test() = "Working"

    @PostMapping(URLpath.register)
    fun registerUser(@RequestBody authRequest: RegistrationRequest): ResponseEntity<AuthenticationResponse> {
        sdUserRepository.findByEmail(authRequest.email)?.let {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .header(Error.headerKey, Error.emailExists).build()
        }

        sdUserRepository.findByUserName(authRequest.username)?.let {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .header(Error.headerKey, Error.userNameExists).build()
        }

        authRequest.roles.split(",").forEach {
            if (!ROLES.contains(it)) {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .header(Error.headerKey, Error.invalidRole).build()
            }
        }

        var hashedPassword = BCryptPasswordEncoder().encode(authRequest.password)
        val user = SDUser(userName = authRequest.username,
                password = hashedPassword,
                isActive = true,
                roles = authRequest.roles,
                email = authRequest.email,
                imageUrl = authRequest.imageUrl,
                phoneNumber = authRequest.phoneNumber)
        sdUserRepository.save(user)

        val userDetails = SDUserDetails(user)
        val jwt = jwtTokenUtil.generateToken(userDetails)

        return ResponseEntity.status(HttpStatus.CREATED).body(AuthenticationResponse((jwt)))
    }

    @PostMapping(URLpath.verify + URLpath.email)
    fun verify(@RequestParam @Email email: String): ResponseEntity<Void> {
        sdUserRepository.findByEmail(email)?.let {
            return ResponseEntity.status(HttpStatus.CONFLICT).build()
        }

        return sendTo(email)
//        return ResponseEntity.ok().build()
    }

    @PostMapping(URLpath.authenticate)
    fun createAuthenticationToken(@RequestBody loginRequest: LoginRequest): ResponseEntity<AuthenticationResponse> {

        var user: SDUser = sdUserRepository.findByUserName(loginRequest.identifier)?.let {
            it
        } ?: sdUserRepository.findByEmail(loginRequest.identifier)?.let {
            it
        } ?: return ResponseEntity.notFound().header(Error.headerKey, Error.userNotFound).build()

        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(user.userName, loginRequest.password))

        val userDetails = userDetailsService.loadUserByUsername(user.userName)
        val jwt = jwtTokenUtil.generateToken(userDetails)

        return ResponseEntity.ok(AuthenticationResponse(jwt))
    }

    @GetMapping(URLpath.profile)
    fun getUser(): UserDTO? {
        var user = Helper.getUser() ?: return null
        return UserDTO(user.id, user.userName, user.roles, user.email)
    }

    @GetMapping(URLpath.master + URLpath.profile)
    fun getUsers(): List<UserDTO> {
        return sdUserRepository.findAll().map { UserDTO(it.id, it.userName, it.roles, it.email) }
    }

    @PatchMapping(URLpath.deactivate)
    fun deactivate(): ResponseEntity<Void> {
        var user = Helper.getUser() ?: return ResponseEntity.status(HttpStatus.FORBIDDEN).build()
        var count = sdUserRepository.updateState(user.id, false)

        if (count == 1) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build()
        }

        return ResponseEntity.badRequest().build()
    }

    private fun sendTo(email: String): ResponseEntity<Void> {
        val token = jwtTokenUtil.confirmationToken(email)
//        val baseURL = Helper.getBaseURL()
        var fullURL = "${Helper.getBaseURL()}${URLpath.confirm + URLpath.email}?token=$token"

        var message = SimpleMailMessage()
        message.setTo(email)
        message.setSubject("Confirm your email - Dapp")
        message.setText("To confirm your e-mail address, please click the link below:\n"
                + fullURL)
        message.setFrom("noreply@skydevz.com")
//        javaMailSender.send(message)
        return ResponseEntity.status(HttpStatus.FOUND)
                .location(URI.create(fullURL))
                .build()
//        println(message)
//        emailService.sendEmail(message)
//        javaMailSender.send(message)
    }

    @GetMapping(URLpath.confirm + URLpath.email)
    fun confirmEmail(@RequestParam token: String): ResponseEntity<Void> {
        jwtTokenUtil.getEmailFrom(token)?.let {
            return ResponseEntity.ok().build()
        }

        return ResponseEntity.badRequest().build()
//        return ResponseEntity.status(HttpStatus.FOUND)
//                .location(URI.create("http://www.google.com"))
//                .build()
    }
}