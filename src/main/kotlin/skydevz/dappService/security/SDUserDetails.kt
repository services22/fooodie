package skydevz.dappService.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import skydevz.dappService.common.SDUserRepository

@Service
class SDUserDetailsService(private val sdUserRepository: SDUserRepository) : UserDetailsService {


    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(userName: String): UserDetails {

        sdUserRepository.findByUserName(userName)?.let {
            return SDUserDetails(it)
        }

        throw  UsernameNotFoundException("Please provide a valid user name")
    }
}

class SDUserDetails(val user: SDUser) : UserDetails {

    val id: String = user.id
//    private val userName: String = user.userName
//    private val password: String = user.password
//    private val active: Boolean = user.isActive
    private val authorities: List<GrantedAuthority>

    init {
        this.authorities = user.roles.split(",").map { SimpleGrantedAuthority(it) }
    }

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return authorities
    }

    override fun getPassword(): String {
        return user.password
    }

    override fun getUsername(): String {
        return user.userName
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return user.isActive
    }
}
