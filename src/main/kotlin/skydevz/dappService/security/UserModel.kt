package skydevz.dappService.security

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column

@Entity @Table
class Admin (
        @Id @Column var id: String,
        @Column var shopID: String,
        @Column var signUpType: SignUpType = SignUpType.EMAIL,
        @Column var signUpDate: Date,
        @Column var planStartDate: Date,
        @Column var planValidity: Int, // number of days
        @Column var planType: PlanType = PlanType.FREE) {
    enum class PlanType {
        FREE, BASIC, MEDIUM, PREMIUM
    }

    enum class SignUpType {
        EMAIL, Google, Facebook
    }
}

@Entity @Table
class Staff(
        @Id @Column var id: String,
//        @Column var shopID: String,
        @Column var adminID: String,
        @Column var type: Type = Type.SERVER) {
    enum class Type {
        WAITER, SERVER
    }
}

@Entity @Table
class Customer(
        @Id @Column var id: String,
        @Column var deviceID: String?,
        @Column var signUpType: SignUpType = SignUpType.DEVICE) {
    enum class SignUpType {
        DEVICE, EMAIL
    }
}