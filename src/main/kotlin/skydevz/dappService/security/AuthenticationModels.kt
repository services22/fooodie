package skydevz.dappService.security

import skydevz.dappService.common.SDRole
import java.util.*
import javax.persistence.*


@Entity @Table
data class SDUser (
        @Id @Column var id: String = UUID.randomUUID().toString(),
        @Column var userName: String,
        @Column var password: String,
        @Column var isActive: Boolean,
        @Column var roles: String,
        @Column var email: String,
        @Column var imageUrl: String?,
        @Column var phoneNumber: Long?
) {
    fun getRole(): SDRole {
        if (roles.contains(SDRole.MASTER.name)) {
            return SDRole.MASTER
        }

        if (roles.contains(SDRole.ADMIN.name)) {
            return SDRole.ADMIN
        }

        if (roles.contains(SDRole.WAITER.name)) {
            return SDRole.WAITER
        }

        if (roles.contains(SDRole.SERVER.name)) {
            return SDRole.SERVER
        }

        return SDRole.USER
    }
}

//@Service
//class EmailService(val mailSender: JavaMailSender) {
//
//    @Async
//    fun sendEmail(email: SimpleMailMessage) {
//        mailSender.send(email)
//    }
//}