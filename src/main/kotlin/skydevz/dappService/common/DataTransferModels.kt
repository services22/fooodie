package skydevz.dappService.common

import com.fasterxml.jackson.annotation.JsonProperty
import skydevz.dappService.address.Address
import skydevz.dappService.cart.Cart
import skydevz.dappService.category.Category
import skydevz.dappService.item.Item
import skydevz.dappService.place.Place
import skydevz.dappService.shop.*
import skydevz.dappService.tax.Tax
import java.util.*
import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.PrePersist
import javax.persistence.PreUpdate

@MappedSuperclass
abstract class TimeStampedEntity {

    @Column(nullable = false, updatable = false)
    @JsonProperty
    protected var createdTime: Long? = null

    @Column(nullable = false)
    @JsonProperty
    protected var modifiedTime: Long? = null

    @PrePersist
    protected fun onCreate() {
        createdTime = Date().time
        modifiedTime = createdTime
    }

    @PreUpdate
    protected fun onUpdate() {
        modifiedTime = Date().time
    }
}

class RegistrationRequest(var username: String,
                          var password: String,
                          var roles: String,
                          var email: String,
                          var imageUrl: String?,
                          var phoneNumber: Long?)

class AuthenticationResponse(var jwt: String)

data class LoginRequest(var identifier: String, var password: String)

data class ShopRequest(var name: String,
                       var imageUrl: String?,
                       var placeType: PlaceType = PlaceType.RESTAURANT,
                       var taxBasedOn: TaxingType = TaxingType.SHOP)

class CartWithItems(var cart: Cart, var items: ArrayList<CartItemDTO>, var user: UserInfo, var place: Place)

class CartDTO(var cart: Cart, var totalItems: Int, var deliveredItems: Int, var user: UserInfo, var place: Place)

class CartItemDTO(var id: Long, var item: Item, var count: Int, var isDelivered: Boolean)

class UserDTO(var id: String, var username: String, var roles: String, var email: String)

class UserInfo(var name: String, var imageURL: String?)

class ShopDetail(
        var shop: Shop,
        var taxes: List<Tax>? = null,
        var addresses: List<Address>? = null,
        var places: List<Place>? = null,
        var categories: List<Category>? = null
)

