package skydevz.dappService.common

val ROLES = arrayOf(SDRole.MASTER.name, SDRole.ADMIN.name, SDRole.WAITER.name, SDRole.SERVER.name, SDRole.USER.name)

enum class SDRole {
    MASTER, ADMIN, WAITER, SERVER, USER
}

class URLpath {

    companion object {

        private const val shopID = "/shop/{shopID}"

        const val api = "/api"
//        const val api1 = "$api/v1"
//        const val api2 = "$api/v2"
        const val id = "/{id}"

        // authentication related endpoints
        const val register = "/register"
        const val verify = "/verify"
        const val confirm = "/confirm"
        const val email =  "/email"
        const val authenticate = "/authenticate"
        const val profile = "/profile"
        const val profiles = "/profiles"
        const val deactivate = "/deactivate"

        // role based paths
        const val admin = "/admin"
        const val user = "/user"
        const val waiter = "/order"
        const val server = "/server"
        const val master = "/master"
        const val staff = "/staff"

        const val shop = "/shop"
        const val nearByShops = "$shop/nearBy"
        const val shopDetail = "$shop$id/detail"

        const val address = "$shopID/address"
//        const val address = "$addresses/{id}"

        const val places = "$shopID/place"
        const val postPlaces = "$shopID/places"
        const val place = "$places$id"
        const val selectPlace = "$place/selectPlace"


        const val categories = "$shopID/category"
        const val category = "$categories$id"

        const val categoryItems = "$categories/{categoryID}/item"
        const val shopItems = "$shopID/item"
        const val categoryItem = "$categoryItems$id"
        const val shopItem = "$shopItems$id"

        const val carts = "$shopID/cart"
        const val userCarts = "$user/cart"
        const val addItems = "$places/{placeID}"
        const val cart = "$shopID/cart$id"
        const val cartItem = "$carts/{cartID}/cartItem$id"
        const val itemDeliver = "$shopID/cartItem$id"
        const val changePlace = "$cart/changePlace"
        const val itemDelivered = "$cartItem/delivered"

        const val bill = "$carts/{cartID}/checkout"

        const val taxes = "$shopID/tax"
        const val tax   = "$shopID/tax/{taxId}"

    }
}

class Error {
    companion object {
        const val headerKey = "Error"

        const val userNameExists = "userNameExists"
        const val emailExists = "emailExists"
        const val userNotFound = "userNotFound"
        const val invalidRole = "invalidRole"
        const val maxShopLimit = "maxShopLimit"
        const val shopNotFound = "shopNotFound"
        const val shopMismatch = "shopIdMismatch"
        const val categoryNotFound = "categoryNotFound"
        const val categoryMismatch = "categoryMismatch"
        const val placeNotFound = "placeNotFound"
        const val placeMismatch = "placeMismatch"
        const val itemNotFound = "itemNotFound"
    }
}
