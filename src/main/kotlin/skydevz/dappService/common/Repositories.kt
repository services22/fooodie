package skydevz.dappService.common

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import skydevz.dappService.address.Address
import skydevz.dappService.category.Category
import skydevz.dappService.item.Item
import skydevz.dappService.place.Place
import skydevz.dappService.security.SDUser
import skydevz.dappService.shop.Shop
import skydevz.dappService.tax.ParentType
import skydevz.dappService.tax.Tax
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import skydevz.dappService.cart.*
import skydevz.dappService.security.*
import javax.transaction.Transactional

@Repository
interface SDUserRepository: JpaRepository<SDUser, String> {
    // returning nullable user means that default internal server error will not be thrown
    fun findByUserName(userName: String): SDUser?
    fun findByEmail(email: String): SDUser?

    @Modifying @Transactional
    @Query("UPDATE SDUser s SET s.isActive = :isActive WHERE s.id = :id")
    fun updateState(@Param("id") id: String, @Param("isActive") isActive: Boolean): Int
}

@Repository
interface AdminRepository: JpaRepository<Admin, String>

@Repository
interface StaffRepository: JpaRepository<Staff, String> {
//    @Query("SELECT * FROM Shop s WHERE s.id = (SELECT a.shopID FROM Admin a WHERE a.id = (SELECT st.adminID FROM Staff st WHERE st.id = :staffID))")
//    fun getShops(staffID: String): List<Shop>
}

@Repository
interface CustomerRepository: JpaRepository<Customer, String>


@Repository
interface ShopRepository : JpaRepository<Shop, String> {
    @Query("SELECT * FROM shop WHERE adminID = :adminID AND modified_time > :modifiedTime", nativeQuery = true)
    fun getShops(adminID: String, modifiedTime: Long): List<Shop>

    fun getShopsByAdminID(adminID: String): List<Shop>
}

@Repository
interface AddressRepository : JpaRepository<Address, Long> {
    fun getAddressListByShopID(shopID: String): List<Address>
    fun getAddressByPostalCodeOrCityOrLatitudeAndLongitude(postalCode: Int?, city: String?,
                                                           latitude: Double?, longitude: Double?): List<Address>?
    fun getAddressesByLatitudeBetweenAndLongitudeBetween(minLat: Double, maxLat: Double, minLon: Double, maxLon: Double): List<Address>
}

@Repository
interface PlaceRepository : JpaRepository<Place, Long> {
    fun getPlacesByShopID(shopID: String): List<Place>
    fun getPlacesByShopIDAndIsAvailable(shopId: String, isAvailable: Boolean): List<Place>

    @Modifying @Transactional
    @Query("UPDATE Place p SET p.isAvailable = :isAvailable WHERE p.id = :id")
    fun updateAvailablity(@Param("id") id: Long, @Param("isAvailable") isAvailable: Boolean): Int
}

@Repository
interface CategoryRepository : JpaRepository<Category, Long> {
    fun getCategoriesByShopID(shopId: String): List<Category>

    @Modifying @Transactional
    @Query("UPDATE Category c SET c.isAvailable = :isAvailable WHERE c.id = :id")
    fun updateAvailablity(@Param("id") id: Long, @Param("isAvailable") isAvailable: Boolean): Int

//    @Modifying @Transactional
//    @Query("UPDATE Category c SET c.position = NULL WHERE c.position = :position")
//    fun nullCategoryPosition(@Param("position") position: Int)
//
//    @Modifying @Transactional
//    @Query("UPDATE Category c SET c.position = :position WHERE c.id = :id")
//    fun updatePosition(@Param("id") id: Long, @Param("position") position: Int): Int
}

@Repository
interface ItemRepository : JpaRepository<Item, Long> {
    fun getItemByShopIDAndCategoryID(shopId: String, categoryId: Long): List<Item>
    fun getItemsByShopIDAndIsAvailable(shopId: String, isAvailable: Boolean): List<Item>
    fun getItemsByShopID(shopId: String): List<Item>

    @Modifying @Transactional
    @Query("UPDATE Item i SET i.price = :price WHERE i.id = :id")
    fun updatePrice(@Param("id") id: Long, @Param("price") price: Double): Int

    @Modifying @Transactional
    @Query("UPDATE Item i SET i.isAvailable = :isAvailable WHERE i.id = :id")
    fun updateAvailablity(@Param("id") id: Long, @Param("isAvailable") isAvailable: Boolean): Int
}

@Repository
interface TaxRepository : JpaRepository<Tax, Long> {
    fun getTaxesByParentTypeAndParentID(parentType: ParentType, parentId: String): List<Tax>
}

@Repository
interface CartRepository : JpaRepository<Cart, Long> {
    fun getCartsByShopID(shopID: String): List<Cart>
    fun getCartsByUserID(userID: String): List<Cart>
    fun getCartsByShopIDAndStatus(shopId: String, status: Cart.Status): List<Cart>

    @Modifying @Transactional
    @Query("UPDATE Cart c SET c.placeID = :placeID WHERE c.id = :cartID")
    fun updatePlace(@Param("cartID") cartID: Long, @Param("placeID") placeID: Long): Int

}

@Repository
interface CartItemRepository : JpaRepository<CartItem, Long> {
    fun getCartItemsByCartID(cartID: Long): List<CartItem>

    @Modifying @Transactional
    @Query("UPDATE CartItem c SET c.isDelivered = :isDelivered WHERE c.id = :id")
    fun updateDeliveryStatus(@Param("id") id: Long, @Param("isDelivered") isDelivered: Boolean): Int
}

@Repository
interface BillRepository : JpaRepository<Bill, Long>

