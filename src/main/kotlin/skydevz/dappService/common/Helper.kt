package skydevz.dappService.common

import org.springframework.data.repository.CrudRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import skydevz.dappService.security.SDUser
import skydevz.dappService.security.SDUserDetails
import javax.servlet.http.HttpServletRequest

class Helper {
    companion object {
        fun getBaseURL(): String {
            val requestAttributes = RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes
            return requestAttributes.request.getBaseUrl()
        }

        fun getUser(): SDUser? { return getUserDetails()?.user }

        fun getUserDetails(): SDUserDetails? {
            (SecurityContextHolder.getContext().authentication.principal as? SDUserDetails)?.let {
                return it
            }

            return null
        }
    }
}

class Range<T>(var min: T, var max: T)


sealed class Result<out Success, out Failure>

data class Success<out Success>(val value: Success) : Result<Success, Nothing>()
data class Failure<out Failure>(val error: Failure) : Result<Nothing, Failure>()

fun String.isCurrentUserID(): Boolean {
    return this == Helper.getUserDetails()?.id
}

fun String.checkCurrentUser() {
    if (!isCurrentUserID()) {
        throw org.springframework.security.access.AccessDeniedException("403 returned")
    }
}

fun HttpServletRequest.getBaseUrl(): String {
    return this.requestURL.substring(0, this.requestURL.length - this.requestURI.length) + this.contextPath
}

open class BaseShopController(val shopRepository: ShopRepository) {

    fun checkShop(shopIDinPath: String, shopIDinBody: String): ResponseEntity.BodyBuilder? {
        checkShopExists(shopIDinPath)?.let { return it }
        checkAdminAccess(shopIDinPath)?.let { return it }

        return compareShopIDs(shopIDinPath, shopIDinBody)
    }

    fun checkAdminAccess(shopID: String): ResponseEntity.BodyBuilder? {
        if (Helper.getUserDetails()?.id != shopRepository.getOne(shopID).adminID) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
        }

        return null
    }

    fun checkShopExists(shopID: String): ResponseEntity.BodyBuilder? {

        if (!shopRepository.existsById(shopID)) {
            return ResponseEntity.badRequest().header(Error.headerKey, Error.shopNotFound)
        }

        return null
    }

    fun compareShopIDs(leftID: String, rightID: String): ResponseEntity.BodyBuilder? {

        if (leftID != rightID) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).header(Error.headerKey, Error.shopMismatch)
        }

        return null
    }
}

fun<T, ID> CrudRepository<T, ID>.getById(id: ID): T? {
    var result = findById(id)

    if (result.isPresent) {
        return result.get()
    }

    return  null
}